class DadJoke {
  final String id;
  final String joke;
  final int status;

  DadJoke({required this.id, required this.joke, required this.status});

  factory DadJoke.fromJson(Map<String, dynamic> json) => DadJoke(
    id: json['id'],
    joke: json['joke'],
    status: json['status']
  );
}