import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'model/dad_joke.dart';

void main() {
  runApp(const RandomJokes());
}

class RandomJokes extends StatefulWidget {
  const RandomJokes({Key? key}) : super(key: key);

  @override
  State<RandomJokes> createState() => _MyAppState();
}

class _MyAppState extends State<RandomJokes> {
  late Future<DadJoke> dadJoke;

  @override
  void initState() {
    super.initState();
    dadJoke = fetchRandomJoke();
  }

  updateRandomJoke() {
    setState(() {
      dadJoke = fetchRandomJoke();
    });
  }

  Future<DadJoke> fetchRandomJoke() async {
    var response = await http.get(
      Uri.parse('https://icanhazdadjoke.com/'),
      headers: {'Accept': 'application/json'},
    );

    if (response.statusCode == 200) {
      return DadJoke.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to fetch dad joke');
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Random Jokes'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FutureBuilder<DadJoke>(
                future: dadJoke,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(
                      snapshot.data!.joke,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    );
                  } else if (snapshot.hasError) {
                    return Text("$snapshot.error");
                  }

                  return const CircularProgressIndicator();
                },
              ),
              const SizedBox(
                height: 30.0,
              ),
              ElevatedButton(
                onPressed: updateRandomJoke,
                child: const Text("Get Random Dad Joke",
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
